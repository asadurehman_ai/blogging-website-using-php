<?php
    //Starting Session
    session_start();

    if(isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == true){
        header('Location: index.php');
        exit();
    }

    //Including Database Configs
    include('config.php');

    //Checking is there is a POST Request
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
        $email = $_POST['email'];
        $password = $_POST['password'];

        //Checking Existing Email
        $sql = "SELECT * FROM user where email='$email' and password='$password'";
        $result = mysqli_query($conn, $sql);
        
        if(empty(mysqli_num_rows($result))){
            header("Location: login.php?error=No Account Found!");
            exit();
        }

        $row = mysqli_fetch_array($result);

        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['full_name'] = $row['full_name'];
        $_SESSION['isLoggedIn'] = true;

        header('Location: index.php');
    }
?>


<html>
    <head>
        <title>Login to Blogging Site</title>
        <link rel="stylesheet" href="./css/login.css">
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <?php
            if(isset($_GET['error'])){
                echo "
                <div class='error-message'>
                    <div class='message'>
                        <span>".$_GET['error']."</span>
                    </div>
                </div>
                ";
            }
        ?>
        <div class='form'>
            <h2 class='form-heading'>Login</h2>
            <form action="login.php" method='post'>
                <div class='form-item'>
                    <label>Email Address</label>
                    <input type='email' name='email' required>
                </div>
                <div class='form-item'>
                    <label>Password</label>
                    <input type='password' name='password' required>
                </div>
                <div class='form-item'>
                    <input type='submit' value='Login'>
                </div>
            </form>
        </div>
    </body>
</html>