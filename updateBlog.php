<?php 
    //Starting Session
    session_start();

    if(!isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == false){
        header('Location: login.php');
        exit();
    }

    //Including Database Configs
    include('config.php');

    if(!isset($_GET['blog_id'])){
        header('Location: index.php');
        exit();
    }

    $blog_id = $_GET['blog_id'];

    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $user_id = $_SESSION['user_id'];

        // GETTING BLOG
        $sql = "SELECT * FROM blog where blog_id=$blog_id";
        $result = mysqli_query($conn, $sql);

        //CHECKING IF BLOG EXIST
        if(empty(mysqli_num_rows($result))){
            header("Location: index.php");
            exit();
        }
        
        // Checking if authentic user is interacting with the blog
        $row = mysqli_fetch_array($result);
        if($row['user_id'] != $user_id){
            header('Location: index.php');
            exit();
        }
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $title = $_POST['title'];
        $content = $_POST['content'];

        $sql = "UPDATE blog SET blog_name='$title', blog_content='$content' WHERE blog_id=$blog_id";
        $result = mysqli_query($conn, $sql);

        header('Location: index.php?success=Blog Added Successfully');
    }

?>
<html>
    <head>
        <title>Update Blog</title>
        <link rel='stylesheet' href='./css/addBlog.css'>
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <div class='addBlogSection'>
            <h2>Update Blog</h2>
            <form action='updateBlog.php?blog_id=<?php echo $blog_id; ?>' method='post'>
                <div class='form-item'>
                    <label>Blog Title</label>
                    <input type='text' name='title' value='<?php echo $row['blog_name'] ?>' required>
                </div>
                <div class='form-item content-input'>
                    <label>Blog Content</label>
                    <textarea name='content' required><?php echo $row['blog_content'] ?></textarea>
                </div>
                <div class='form-item'>
                    <input type='submit' value='Publish'>
                </div>
            </form>
        </div>
        <div class='faq'>
            <h3>Frequently Asked Questions</h3>
            <div id='faq'></div>
        </div>
        <script src='getFaq.js'></script>
    </body>
</html>