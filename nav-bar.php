<nav class="nav-bar">
    <div>
        <div class="nav-bar-header">
            <h3>Asad Blogging Site</h3>
            <span>Adiyala Road, Rawalpindi</span>
        </div>
        <div class="nav-bar-items">
            <?php 
                if(isset($_SESSION['isLoggedIn']) == false || $_SESSION['isLoggedIn'] == false){
                    echo '
                        <a href="./login.php">Login</a>
                        <a href="./register.php">Register</a>
                    ';
                }else{
                    echo '
                        <a href="./index.php">Home</a>
                        <a href="./addBlog.php">Add Blog</a>
                        <a href="./ownBlog.php">My Blogs</a>
                        <a href="./logout.php">Logout</a>
                        ';
                }
            ?>
        </div>
    </div>
</nav>