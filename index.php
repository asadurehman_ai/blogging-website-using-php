<?php
    //Starting Session
    session_start();

    if(!isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == false){
        header('Location: login.php');
        exit();
    }

    include 'config.php';

    $fullname=$_SESSION['full_name'];
?>
<html>
    <head>
        <title>Blogs</title>
        <link rel="stylesheet" href="./css/index.css">
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <div class='heading'>
            <h2>Welcome <?php echo $fullname; ?></h2>
            <h3>Top Blogs</h3>
        </div>
        <div class='blog-section'>
            <?php
                $sql = "SELECT blog_id, blog_name, blog_content, full_name from blog inner join user on blog.user_id = user.user_id order by blog_id desc";
                $result = mysqli_query($conn, $sql);

                while($row = mysqli_fetch_array($result)){
                    echo "
                        <div class='blog'>
                            <div class='blog-info'>
                                <h3 class='title'>".substr($row[1], 0, 50)."...</h3>
                                <span class='fullname'>$row[3]</span>
                            </div>
                            <div class='blog-content'>
                                <p>".substr($row[2],0,100)."...</p>
                            </div>
                            <div class='blog-read'>
                                <a href='./readBlog.php?blog_id=$row[0]'>Read</a>
                            </div>
                        </div>
                    ";
                }
            ?>
        </div>

        <div class='famous-authors'>
            <h3>Famous Authors</h3>
            <div id='famous-authors'>
            </div>
        </div>

        <div class='faq'>
            <h3>Frequently Asked Questions</h3>
            <div id='faq'></div>
        </div>

        <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
        <script src='getFaq.js'></script>
        <script src='getFamousAuthors.js'></script>
    </body>
</html>