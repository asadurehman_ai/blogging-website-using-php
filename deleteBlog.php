<?php
    //Starting Session
    session_start();

    if(!isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == false){
        header('Location: login.php');
        exit();
    }

    //Including Database Configs
    include('config.php');

    if(!isset($_GET['blog_id'])){
        header('Location: index.php');
        exit();
    }

    $blog_id = $_GET['blog_id'];
    $user_id = $_SESSION['user_id'];

    // GETTING BLOG
    $sql = "SELECT blog_id, user_id FROM blog where blog_id=$blog_id";
    $result = mysqli_query($conn, $sql);

    //CHECKING IF BLOG EXIST
    if(empty(mysqli_num_rows($result))){
        header("Location: index.php");
        exit();
    }
    
    // Checking if authentic user is interacting with the blog
    $row = mysqli_fetch_array($result);
    if($row['user_id'] != $user_id){
        header('Location: index.php');
        exit();
    }

    // Deleting the blog
    $sql = "DELETE FROM blog where blog_id=$blog_id";
    $result = mysqli_query($conn, $sql);

    header('Location: ownBlog.php');
?>