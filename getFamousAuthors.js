$(document).ready(function(){
    $.ajax({
     type: "GET" ,
     url: "author.xml" ,
     dataType: "xml" ,
     success: function(xml) {
        $(xml).find('author').each(function() {
            var name = $(this).find('name').text();
            var book = $(this).find('book').text();
            var profile_pic = $(this).find('profile_pic').text();
            
            $('#famous-authors').append(
                `<div class='author'>
                    <div>
                        <img src='${profile_pic}'>
                    </div>
                    <h3>${name}</h3>
                    <p>${book}</p>
                </div>`
            );
        });
     }       
 });
 });