<?php

    session_start();

    //Including Database Configs
    include('config.php');

    if(!isset($_GET['blog_id'])){
        header('Location: index.php');
        exit();
    }

    $blog_id = $_GET['blog_id'];
    $sql = "SELECT blog_id, blog_name, blog_content, full_name from blog inner join user on blog.user_id = user.user_id where blog_id = $blog_id";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result)
?>

<html>
    <head>
        <title></title>
        <link rel='stylesheet' href='./css/readBlog.css'>
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <div class='blog'>
            <h2><?php echo $row[1] ?></h2>
            <p><?php echo $row[2] ?></p>
        </div>
        <div class='faq'>
            <h3>Frequently Asked Questions</h3>
            <div id='faq'></div>
        </div>
        <script src='getFaq.js'></script>
    </body>
</html>