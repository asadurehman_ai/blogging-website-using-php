<?php
    //Starting Session
    session_start();

    if(isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == true){
        header('Location: index.php');
        exit();
    }

    //Including Database Configs
    include('config.php');

    //Checking is there is a POST Request
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
        $full_name = $_POST['fname'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        //Validating Password
        if($password != $_POST['c_password']){
            header("Location: register.php?error=Password Didnot Match!");
            exit();
        }

        //Checking Existing Email
        $sql = "SELECT * FROM user where email='$email'";
        $result = mysqli_query($conn, $sql);
        
        if(!empty(mysqli_num_rows($result))){
            header("Location: register.php?error=Account Already Exsist!");
            exit();
        }

        //Inserting Record
        $sql = "INSERT INTO user (full_name, email, password) VALUES ('$full_name', '$email', '$password')";
        $result = mysqli_query($conn, $sql);

        $_SESSION['user_id'] = $conn->insert_id;
        $_SESSION['full_name'] = $full_name;
        $_SESSION['isLoggedIn'] = true;

        header('Location: index.php');
    }
?>

<html>
    <head>
        <title>Register to Blogging Site</title>
        <link rel="stylesheet" href="./css/login.css">
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <?php
            if(isset($_GET['error'])){
                echo "
                <div class='error-message'>
                    <div class='message'>
                        <span>".$_GET['error']."</span>
                    </div>
                </div>
                ";
            }
        ?>
        <div class='form'>
            <h2 class='form-heading'>Register</h2>
            <form action='register.php' method='post'>
                <div class='form-item'>
                    <label>Full Name</label>
                    <input type='text' name='fname' required>
                </div>
                <div class='form-item'>
                    <label>Email Address</label>
                    <input type='email' name='email' required>
                </div>
                <div class='form-item'>
                    <label>Password</label>
                    <input type='password' name='password' required>
                </div>
                <div class='form-item'>
                    <label>Confirm Password</label>
                    <input type='password' name='c_password' required>
                </div>
                <div class='form-item'>
                    <input type='submit' value='Login'>
                </div>
            </form>
        </div>
    </body>
</html>