<?php 
    //Starting Session
    session_start();

    if(!isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == false){
        header('Location: login.php');
        exit();
    }

    //Including Database Configs
    include('config.php');

?>
<html>
    <head>
        <title>Add New Blog</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
        <link rel='stylesheet' href='./css/index.css'>
        <style>
            .blog-options{
                display: flex;
                gap: 10px;
                font-size: 18px;
            }

            .blog-options a{
                color: gray;
            }

            .bi-x-circle:hover{
                color: red;
            }
            .bi-pencil-square:hover{
                color: green;
            }
        </style>
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <div class='heading'>
            <h2>My Blogs</h2>
        </div>
        <div class='blog-section'>
            <?php
                $user_id = $_SESSION['user_id'];
                $sql = "SELECT blog_id, blog_name, blog_content, full_name from blog inner join user on blog.user_id = user.user_id where blog.user_id=$user_id order by blog_id desc";
                $result = mysqli_query($conn, $sql);

                while($row = mysqli_fetch_array($result)){
                    echo "
                        <div class='blog'>
                            <div class='blog-info'>
                                <h3 class='title'>".substr($row[1], 0, 50)."...</h3>
                                <span class='fullname'>$row[3]</span>
                                <div class='blog-options'>
                                    <a href='./updateBlog.php?blog_id=$row[0]'><i class='bi bi-pencil-square'></i></a>
                                    <a href='./deleteBlog.php?blog_id=$row[0]'><i class='bi bi-x-circle'></i></a> 
                                </div>
                            </div>
                            <div class='blog-content'>
                                <p>".substr($row[2],0,100)."...</p>
                            </div>
                            <div class='blog-read'>
                                <a href='#'>Read</a>
                            </div>
                        </div>
                    ";
                }
            ?>
        </div>

        <div class='faq'>
            <h3>Frequently Asked Questions</h3>
            <div id='faq'></div>
        </div>
        <script src='getFaq.js'></script>
    </body>
</html>