var faqs;

function string_to_json(jsonString){
    faqs = JSON.parse(jsonString);
}

function addFaqs(){
    var content = ``
    faqs.forEach(element => {
        content += `
            <div class='faq-question'>
                <h3>${element['question']}</h3>
                <p>${element['answer']}</p>
            </div>
        `
    });
    document.getElementById('faq').innerHTML = content;
}

// Creating XML Object
var xhr = new XMLHttpRequest();

// Setting Request Methods and Location
xhr.open('GET', './faq.json', true);

// Sending Request to get data
xhr.send();

xhr.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        string_to_json(this.response);
        addFaqs();
    }
}