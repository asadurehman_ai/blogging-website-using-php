<?php 
    //Starting Session
    session_start();

    if(!isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == false){
        header('Location: login.php');
        exit();
    }

    //Including Database Configs
    include('config.php');

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $title = $_POST['title'];
        $content = $_POST['content'];
        $user_id = $_SESSION['user_id'];

        $sql = "INSERT INTO blog (blog_name, blog_content, user_id) VALUES ('$title', '$content', '$user_id')";
        $result = mysqli_query($conn, $sql);

        header('Location: index.php?success=Blog Added Successfully');
    }

?>
<html>
    <head>
        <title>Add New Blog</title>
        <link rel='stylesheet' href='./css/addBlog.css'>
    </head>
    <body>
        <?php include 'nav-bar.php' ?>
        <div class='addBlogSection'>
            <h2>Add New Blog</h2>
            <form action='addBlog.php' method='post'>
                <div class='form-item'>
                    <label>Blog Title</label>
                    <input type='text' name='title' required>
                </div>
                <div class='form-item content-input'>
                    <label>Blog Content</label>
                    <textarea name='content' required></textarea>
                </div>
                <div class='form-item'>
                    <input type='submit' value='Publish'>
                </div>
            </form>
        </div>
        <div class='faq'>
            <h3>Frequently Asked Questions</h3>
            <div id='faq'></div>
        </div>
        <script src='getFaq.js'></script>
    </body>
</html>